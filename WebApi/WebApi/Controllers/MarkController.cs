﻿using BusinessLayer;
using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    [RoutePrefix("api/mark")]
    public class MarkController : ApiController
    {
        private MarkBusiness markBusiness;

        public MarkController()
        {
            this.markBusiness = new MarkBusiness();
        }
        [Route("get/{index}")]
        [HttpGet]
        public List<Mark> GetAllStudents(string index)
        {
            return this.markBusiness.GetAllMarks(index);
        }
    }
}