﻿using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class MarkRepository
    {
        private string ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];

        public List<Mark> GetAllMarks(string index)
        {
            List<Mark> listToReturn = new List<Mark>();
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = dataConnection,
                    CommandText = "SELECT s.Name, m.Mark FROM Students st INNER JOIN(Marks m INNER JOIN Subjects s ON m.SubjectsId = s.Id) ON st.Id = m.StudentsId WHERE st.IndexNumber = '" + index + "'"
                };

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Mark m = new Mark
                    {
                        SubjectName = dataReader.GetString(0),
                        MarkValue = dataReader.GetInt32(1)
                    };
                    listToReturn.Add(m);
                }
            }
            return listToReturn;
        }
    }
}