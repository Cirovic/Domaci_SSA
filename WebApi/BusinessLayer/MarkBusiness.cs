﻿using DataLayer;
using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class MarkBusiness
    {
        private MarkRepository markRepository;

        public MarkBusiness()
        {
            this.markRepository = new MarkRepository();
        }

        public List<Mark> GetAllMarks(string index)
        {
            List<Mark> marks = this.markRepository.GetAllMarks(index);
            if (marks.Count > 0)
            {
                return marks;
            }
            else
            {
                return null;
            }
        }
    }
}